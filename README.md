# OD Forms

> A series of components to make building html forms fast and easy.
> NEW in 8.2.0: Dynamic labels!

`<od-form>` is a smart component to wrap other OD Form elements. It offers a series of benefits over semantic html forms while maintaining the ability to interact as one would normally with an html form. Supports semantic html attributes.

`<od-form-array>` is a smart component to wrap other OD Form elements. It creates a series of od-form elements based on a template to return an array of data. It also allows for simple creation and deletion of entries.

`<od-form-input>` is a simple component to create an input element. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes.

`<od-form-textarea>` is a simple component to create a textarea element. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes.

`<od-form-selectlist>` is a simple component to create a select element. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes. Supports both dynamic options and statically defined options.

`<od-form-multiselect>` is a simple component to create a series of checkbox elements. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes. Supports both dynamic options and statically defined options.

`<od-form-radioselect>` is a simple component to create a series of radio elements. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes. Supports both dynamic options and statically defined options.

`<od-form-richtext>` is a simple component to create custom richtext field. Like other Form Elements it comes with a series of styling and functional benefits including built in labels and semantic html attributes.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-forms
```
## Usage
** Please note that your usage may differ depending on your framework **
```
import { OdForm, OdFormInput } from '@orcden/od-forms';
```
```
<od-form id='od-form1'>                 //unique id required
    <od-form-input  name='first-name'   //required
                    autocomplete='given-name'>First Name: </od-form-input>
    
    <od-form-textarea   name='textarea'>Textarea: </od-form-textarea>

    <od-form-selectlist name='select1'
                        options-get='http://myapiroute.com/api/options' //returns [{ optionName: 'option 1', id: '0' },...]
                        options-label='optionName'
                        options-value='id'>
        <label slot='label'>Select1:</label>
        <option value=''>-</option> //statically defined option
    </od-form-selectlist>

    <od-form-multiselect    name='multi'
                            options-get='http://myapiroute.com/api/options'
                            options-label='optionName'
                            options-value='id'>
        <label slot='label'>Multi:</label>
        <od-form-input  value='1st'>1st: </od-form-input> //statically defined option
    </od-form-multiselect>
                
    <od-form-radioselect    name='radio'
                            options-get='http://myapiroute.com/api/options'
                            options-label='optionName'
                            options-value='id'>
        <label slot='label'>Radio:</label>
        <od-form-input  value='1st'>1st: </od-form-input>
    </od-form-radioselect>
</od-form>
```
```
<od-form-array id='od-form-array1'>                 //unique id required
    <od-form-input  name='first-name'               //required
                    autocomplete='given-name'>First Name: </od-form-input>
    
    <od-form-textarea   name='textarea'>Textarea: </od-form-textarea>
                
    <od-form-radioselect    name='radio'
                            options-get='http://myapiroute.com/api/options'
                            options-label='optionName'
                            options-value='id'>
        <label slot='label'>Radio:</label>
        <od-form-input  value='1st'>1st: </od-form-input>
    </od-form-radioselect>
</od-form-array>

//  **Also supports any other OD Form Element**
```
    
## Attributes

### OD Form
| Attribute | Type    | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `disabled`| Boolean | false   | Toggles the hiding of the input fields and the display of the value fields. |
| `inline`  | Boolean | false   | Tells the form to display all of its elements in a single line. Overrides child element inline attributes |
** Also supports any semantic html form attribute


### OD Form Array
| Attribute | Type    | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `disabled`| Boolean | false   | Toggles the hiding of the input fields and the display of the value fields. |
| `inline`  | Boolean | false   | Tells all child forms to display all of its elements in a single line. Overrides child element inline attributes |


### OD Form Input
| Attribute | Type    | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`  | Boolean | false     | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | Two-way binding support to set/get the html input value property |
| `checked` | Boolean | false     | Two-way binding support to set/get the html input checked property |
** Also supports any semantic html input attribute


### OD Form Textarea
| Attribute | Type    | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`  | Boolean | false     | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | Two-way binding support to set/get the html input value property |
** Also supports any semantic html input attribute


### OD Form Selectlist
| Attribute      | Type    | Default | Description                                                                             |
|----------------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`       | Boolean | false   | Tells the element to align its input field inline with its label field |
| `options-get`  | String  | null    | For dynamic options calls. This is the url of your option route |
| `options-label`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `options-value`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |
** Also supports any semantic html input attribute


### OD Form Multiselect
| Attribute      | Type    | Default | Description                                                                             |
|----------------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`       | Boolean | false   | Tells the element to align its input field inline with its label field |
| `options-get`  | String  | null    | For dynamic options calls. This is the url of your option route |
| `options-label`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `options-value`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |
** Also supports any semantic html input attribute


### OD Form Radioselect
| Attribute      | Type    | Default | Description                                                                             |
|----------------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`       | Boolean | false   | Tells the element to align its input field inline with its label field |
| `options-get`  | String  | null    | For dynamic options calls. This is the url of your option route |
| `options-label`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `options-value`| String  | null    | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |
** Also supports any semantic html input attribute

### OD Form Richtext
| Attribute      | Type    | Default | Description                                                                             |
|----------------|---------|---------|-----------------------------------------------------------------------------------------|
| `disabled`       | Boolean | false   | Hides the input field and  |
| `inline`  | Boolean  | false    | Tells the element to align its input field inline with its label field |
| `required`| Boolean  | false    | Lets the form know that the field needs a value before continuing |


## Properties

### OD Form
| Property | Type            | Default         | Description                                                                             |
|-----------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `form`    | HTMLFormElement | HTMLFormElement | Get only. Returns the created html form. Can be used for any semantic html form hooks |
| `disabled`| Boolean         | false           | Toggles the hiding of the input fields and the display of the value fields. |
| `inline`  | Boolean         | false           | Tells the form to display all of its elements in a single line. Overrides child element inline attributes |


### OD Form Array
| Property | Type            | Default         | Description                                                                             |
|-----------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `forms`   | Array           | Array           | Get only. Returns an array of the created html forms. Can be used for any semantic html form hooks |
| `disabled`| Boolean         | false           | Toggles the hiding of the input fields and the display of the value fields. |
| `inline`  | Boolean         | false           | Tells the form to display all of its elements in a single line. Overrides child element inline attributes |


### OD Form Input
| Property   | Type            | Default         | Description                                                                             |
|-------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `input`     | HTMLInputElement| HTMLInputElement| Get only. Returns the created html input. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`  | Boolean | false     | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | get/set the html input value property |
| `checked `  | Boolean | false | get/set the html input checked property |


### OD Form Textarea
| Property   | Type            | Default         | Description                                                                             |
|-------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `input`     | HTMLInputElement| HTMLInputElement| Get only. Returns the created html input. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`    | Boolean         | false           | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | get/set the html input value property |


### OD Form Selectlist
| Property      | Type            | Default         | Description                                                                             |
|----------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `input`        | HTMLInputElement| HTMLInputElement| Get only. Returns the created html input. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`       | Boolean         | false           | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | get/set the current selected value |
| `optionsGet`   | String          | null            | For dynamic options calls. This is the url of your option route |
| `optionsLabel` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `optionsValue` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |


### OD Form Multiselect
| Property      | Type            | Default         | Description                                                                             |
|----------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `inputs`       | Array           |Array            | Get only. Returns an array of the nested inputs. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`       | Boolean         | false           | Tells the element to align its input field inline with its label field |
| `value `  | String/Array | undefined | get/set the current selected values |
| `optionsGet`   | String          | null            | For dynamic options calls. This is the url of your option route |
| `optionsLabel` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `optionsValue` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |


### OD Form Radioselect
| Property      | Type            | Default         | Description                                                                             |
|----------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `inputs`       | Array           |Array            | Get only. Returns an array of the nested inputs. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`       | Boolean         | false           | Tells the element to align its input field inline with its label field |
| `value `  | String | undefined | get/set the current selected value |
| `optionsGet`   | String          | null            | For dynamic options calls. This is the url of your option route |
| `optionsLabel` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's label field  ex. 'optionName'|
| `optionsValue` | String          | null            | For dynamic options calls. This is the field identifier that corresponds to your option's value field ex. 'id' |

### OD Form Richtext
| Property   | Type            | Default         | Description                                                                             |
|-------------|-----------------|-----------------|-----------------------------------------------------------------------------------------|
| `input`     | HTMLInputElement| HTMLInputElement| Get only. Returns the created html input. Can be used for any semantic html form hooks. |
| `labelText` | String          | String ''       | Returns the text of the created html label. Can be used to set label text |
| `inline`    | Boolean         | false           | Tells the element to align its input field inline with its label field |
| `value`  | String | undefined | get/set the html input value property |
| `editor`  | Quill | Quill | Get only. Returns the Quill inhstance for any required hooks |


## Functions

### OD Form
| Name      | returns   | Parameters | Description                                                                             |
|-----------|-----------|-------------|-----------------------------------------------------------------------------------------|
| `submitForm`  | undefined | None        | Triggers an html submit event on the html form with custom validation. |
| `htmlSubmitForm`  | undefined | None        | Triggers an html submit event on the html form with html validation. |
| `reset `  | undefined | None        | Triggers a reset/clearing of the form data |
| `validate`| Boolean   | None        | Cycles through inputs and validates each one. |
| `htmlValidate`| Boolean   | None        | Triggers an html validation of the form. |
| `getData` | Object    | None        | Returns a JSON Object of the FormData associated with the html form. |
| `getValue`| Object    | String name | Returns the current value in the form associated with the given name parameter |
| `setValue`| undefined | String name, Object data | Sets the value in the form associated with the given name parameter |
| `setChecked`| undefined | String name, String value, Boolean isChecked | Sets the associated checkbox or radio in the form associated with the given name parameter |
| `populate`| undefined | Object data | Populates the form with data based on a simple JSON Object. |
| `saveAutocomplete`  | undefined | None        | Triggers the form to save autocomplete data in the browser. |


### OD Form Array
| Name         | returns     | Parameters | Description                                                                             |
|--------------|-------------|-------------|-----------------------------------------------------------------------------------------|
| `addNewEntry`| HTMLElement | None        | Adds a new form based on the template to the DOM. |
| `deleteEntry`| undefined   | Sting id    | Deletes the corresponding form entry from the DOM .|
| `validate`   | Boolean     | None        | Cycles through form entries and validates each one. |
| `getData`    | Array       | None        | Returns a JSON Array of the FormData associated with the html form entries. |
| `populate`   | undefined   | Object data | Populates the form with data based on a simple JSON Array. |
| `reset `     | undefined   | None        | Triggers a reset/clearing of all data in each form |

### Any Form Element
| Name         | returns     | Parameters | Description                                                                             |
|--------------|-------------|-------------|-----------------------------------------------------------------------------------------|
| `listen`   | undefined | eventName, callback  | Adds an event listener to the form element in the ssame way you would a normal listener |


## Styling
- CSS variables are available to alter the default styling provided

| Shadow Part    | Description |
|----------------|-------------|
| form           | Selects the html form |
| table          | The table used for aligning labels and inputs |
| table-row      | Rows under the table part |
| table-cell     | Cells under the rows part |
| label-cell     | Cells under the rows part containing the field label |
| input-cell     | Cells under the rows part containing the input element |
| field-label    | The labels used for identifying separate fields |
| value-label    | The labels used in place of the inputs to display the current value |
| form-input     | Selects inputs in the html form |
| form-select    | Selects 'select' elements in the html form |
| form-textarea  | Seelects Textareas in the form |
| --ARRAY ONLY-- |       |
| array-container| The div used to insert new forms |
| array-fieldset | Selects the wrapper fieldset around each form
| od-form        | Selects the OD Form Elements nested in each fieldset |
| add-button     | The add button used to add new entries |
| delete-button  | The button used to delete corresponding entries |


## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Build for production

```
npm run build
```