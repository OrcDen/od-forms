import ArrayFormBuilder from '../../lib/builders/arrayFormBuilder.js';
import OptionsBuilder from '../../lib/builders/optionsBuilder.js';

//Bugs:
// Indexing issue on delete
// dynamic options on reload - form
// dynamic options in selectlist values

//Features:
//Pseudo delete and add buttons

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host( [disabled] ) #array-container button {
            display: none;
        }

        :host( [disabled] ) button#add {
            display: none;
        }

        #array-container fieldset {
            position: relative;
            width: fit-content;
        }

        #array-container fieldset > button.delete-button {
            display: inline-block;
            position: absolute;
            right: 1em;
            top: 1em;
        }

        #array-container fieldset od-form {
            display: inline-block;
            padding-right: 2em;
        }
        
        #slot {
            display: none;
        }

        #label-slot {
            display: none;
        }

        #add::before {
            content: '+';
        }

        .delete-button::before {
            content: 'x';
        }

    </style>
    
    <div id='array-container' part='array-container'></div>
    <button id='add' part='add-button'></button>
    <slot id='slot'></slot>
    <slot id='label-slot' name='label'></slot>
    <template></template>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-array" );

export class OdFormArray extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    async connectedCallback() {
        this._forms = [];
        this._templateItems = [];
        this._container = this.shadowRoot.querySelector( "#array-container" ); //This must be first

        this._tryBuildLabel();
        
        this._template = this._getTemplate();
        this._arrayFormBuilder = new ArrayFormBuilder( this );
        this._arrayFormBuilder.buildTemplate( this._template, this._templateItems ); 
        this._options = [];
        this._optionsBuilder = new OptionsBuilder( this );

        //set attribute default values first and pass to element
        this._setExportpartsAttribute();
        this._updateAttributes();

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'disabled' );
        this._upgradeProperty( 'inline' );

        //listeners and others etc.
        let addButton = this.shadowRoot.querySelector( '#add' );
        addButton.addEventListener( 'click', () => {
            this.addNewEntry();
        } );

        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => { 
            this._mutationObserverCallback( mutationsList, observer ) 
        } );
        this._observer.observe( this.shadowRoot.querySelector( 'div#array-container' ), config );
    }

    disconnectedCallback() {
        this._observer.disconnect();
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    _getTemplate() {
        let template = this.shadowRoot.querySelector( 'template' );
        this._templateItems = this.shadowRoot.querySelector( '#slot' ).assignedElements();
        for( var i = 0; i < this._templateItems.length; i++ ) {
            let item = this._templateItems[i];
            template.appendChild( item );
        }
        return template;
    }

    _setExportpartsAttribute() {
        this.setAttribute( 'exportparts', 'form, table, table-row, table-cell, label-cell, field-label, field-label-row, value-label ,form-input, form-select, form-textarea, form-richtext, richtext-editor' );
    }

    _updateAttributes() {
        let attributes = this.attributes;
        if( this._template ) {
            this.__updateAttributes( this._template.firstChild.firstChild, attributes );            
        }
        if( !this.forms || this.forms.length < 1 ) {
            return;
        }
        this.forms.forEach( ( form ) => {
            this.__updateAttributes( form, attributes )
        } );            
    }

    __updateAttributes( elem, attributes ) {
        for( var i = 0; i < attributes.length; i++ ) {
            let attribute = attributes[i];
            if( attribute.name === 'part' || attribute.name === 'exportparts' ) {
                continue;
            }
            elem.setAttribute( attribute.name, attribute.value );
        }
        let oldAttributes = elem.attributes;
        for( var i = 0; i < oldAttributes.length; i++ ) {
            let attribute = oldAttributes[i];
            if( !attributes.getNamedItem( attribute.name ) && attribute.name !== 'part' && attribute.name !== 'exportparts' ) {
                elem.removeAttribute( attribute.name );
            }            
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["disabled", "inline"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "disabled": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setDisabledAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setDisabledAttribute( newValue );
                }
                break;
            }
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );
                }
                break;
            }
        }        
        if( newValue !== oldValue ) {
            this._updateAttributes();
        } 
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get forms() {
        return this._forms;
    }    

    get disabled() {
        return this.hasAttribute( "disabled" );
    }

    set disabled( isDisabled ) {
        if ( typeof isDisabled !== "boolean" ) {
            return;
        }
        this._setDisabledAttribute( isDisabled );
    }

    _setDisabledAttribute( newV ) {
        this._setBooleanAttribute( 'disabled', newV );
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
    }   

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'od-form-array-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    } 

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-form-array-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        } );
    }

    _mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-form-array-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    async _getOptionsListener( e ) {
        e.stopPropagation();
        let options = await this._optionsBuilder.getOptions( e.detail.deferred.url );
        e.detail.callback( e.detail.deferred.url, options, e.detail.deferred.id, e.detail.deferred.type, e.detail.deferred.name, e.detail.deferred.required, e.detail.deferred.labelField, e.detail.deferred.valueField );
        
        //these steps are bypassed by using the form options builder and not the array options builder
        this._optionsBuilder.checkUpdateBusy();
        this._optionsBuilder._dispatchOptionsUpdate( e.detail.deferred.url, options );
    }

    _tryBuildLabel() {
        let textNodes = this.shadowRoot.querySelector( '#label-slot' ).assignedNodes( {flatten: true} );        
        if( textNodes.length > 0 ) {
            this._labelText = textNodes[0].innerHTML;
            let fieldset = document.createElement( 'fieldset' );
            let legend = document.createElement( 'legend' );
            legend.innerHTML = this._labelText;
            fieldset.appendChild( legend );
            fieldset.appendChild( this._container );
            fieldset.setAttribute( 'part', 'array-label-fieldset' );
            this.shadowRoot.insertBefore( fieldset, this.shadowRoot.firstChild );
        }
    }

    //Public methods
    async addNewEntry() {
        let newEntry = await this._arrayFormBuilder.addNewEntry( this._template );
        this._forms.push( newEntry );
        return newEntry;
    }

    async deleteEntry( id ) {
        let deleteEntry = await this._arrayFormBuilder.deleteEntry( id );
        this._forms.splice( this._forms.indexOf( deleteEntry ), 1 );
    }

    async deleteAllEntries() {
        let temp = [];
        for( let i = 0; i < this._forms.length; i++ ) {
            temp.push( this._forms[i].parentElement.id );
        }
        for( let i = 0; i < temp.length; i++ ) {
            await this.deleteEntry( temp[i] );
        }
    }

    validate() {
        let valid = true;
        this.forms.forEach( ( form ) => {
            if( !form.validate() ) {
                valid = false;
            }
        } );
        return valid;
    }

    getData() {
        let output = [];
        this.forms.forEach( ( form ) => {
            output.push( form.getData() );
        } );        
        return output;
    }

    async reloadArray() {
        this.forms.forEach( ( form ) => {
            form.reload();
        } );
    }

    resetArray() {
        this.forms.forEach( ( form ) => {
            form.resetForm();
        } );
    }

    async populate( data ) {
        if( !data || !data.length || !data[0] ) {
            return
        }
        await this.deleteAllEntries();
        for( let i = 0; i < data.length; i++ ) {
            await this.addNewEntry();
        }
        for( let i = 0; i < this.forms.length; i++ ) {
            let form = this.forms[i];
            while( !form.ready ) {
                await form.readyPromise();// wait for the form to build its options
            }
            while( this._optionsBuilder.busy ) {
                await form.mutationPromise();// wait for the form to build its options
            }
            form.populate( data[i] );
        }
    }
}
