import OdFormElement from '../../lib/odFormElement.js'

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        label.value-label::before {
            content: "";
            display: inherit;
            padding: .04em;
        }

    </style>
    
    <label for='input' id='label'>
        <slot id='label-slot'></slot>
    </label>
    <input id='input' type='text' >
`;
window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-input" );

export class OdFormInput extends OdFormElement {
    constructor() {
        super( template );
    }

    _init() {
        this._labelSlot = this.shadowRoot.querySelector( '#label-slot' );
        this._input = this.shadowRoot.getElementById( "input" );        
    }

    connectedCallback() {     
        super.connectedCallback();
        this._upgradeInputProperties();
    }

    disconnectedCallback() {        
        super.disconnectedCallback();
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["inline", "value", "checked", "disabled", "hidden", "accept", "alt", "autocomplete", "autofocus", "dirname", 
                "form", "formaction", "formenctype", "formmethod", "formnovalidate", "formtarget", "height", "list", 
                "max", "maxlength", "min", "minlength", "multiple", "pattern", "placeholder", "readonly", "required", 
                "size", "src", "step", "type", "width" ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        if( newValue !== oldValue ) {
            this._updateAttributes();
        } 
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute

    //internal functions should begin with '_'
    _updateAttributes() {
        let label = this.shadowRoot.querySelector( "label" );
        this.__updateAttributes( this.input, label );
    }
}
