import OdFormMultiselectElement from '../../lib/odFormMultiselectElement.js'

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        label.value-label::before {
            content: "";
            display: inherit;
            padding: .04em;
        }

        fieldset {
            width: fit-content;
            white-space: nowrap;
        }

        fieldset label.field-label:not(:first-of-type) {
            margin: 0 0 0 1.5em;
        }

        fieldset label.value-label:not(:nth-of-type(2)) {
            margin: 0 0 0 2em;
        }

    </style>
    
    <slot id='label-slot' name='label'></slot>
    <div id='radio-container'>
        <slot id='input-slot'></slot>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-radioselect" );

export class OdFormRadioselect extends OdFormMultiselectElement {
    constructor() {
        super( template );
    }

    _init() {
        this._labelSlot = this.shadowRoot.querySelector( '#label-slot' );
        this._inputs = [];
        this._getChildren();
        this._setChildAttributes();
    }

    connectedCallback() {
        super.connectedCallback();
    }

    disconnectedCallback() {        
        super.disconnectedCallback();
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["inline", "disabled", "hidden", "required"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        if( newValue !== oldValue ) {
            this._updateAttributes();
        } 

    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get inputs() {
        return this._inputs;
    }

    //internal functions should begin with '_'
    _getChildren() {
        let children = this.shadowRoot.querySelector( '#input-slot' ).assignedNodes();
        for( let i = 0; i < children.length; i++ ) {
            let child = children[i];
            if( child.tagName ) {
                this._inputs.push( child );
            }
        }
    }

    _notifyParent() {
        this.dispatchEvent(
            new CustomEvent( 'od-form-multiselect-get-parent', { 
                bubbles: true, 
                detail: { 
                    'odFormElement': this,
                    'label': this.labelText,
                    'formElements': this.inputs,
                    'callback': ( parent ) => {
                        this._parent = parent;
                        this._dispatchParentDone( this._parent );
                    } 
                }
            } )
        );
    }    

    _updateAttributes() { 
        let input = this.shadowRoot.querySelector( "#radio-container" );
        let label = this.shadowRoot.querySelector( "label" );
        this.__updateAttributes( input, label );
    }

    _setChildAttributes() {
        this.inputs.forEach( ( c ) => {
            if( !c.tagName ) {
                c = c.previousSibling;
            }
            c.setAttribute( 'type', 'radio' );
            c.setAttribute( 'name', this.getAttribute( 'name' ) );
        } )
    }

    getValue() {
        if( !this._parent ) {
            return null;
        }
        return this._parent.getValue( this.getAttribute( 'name' ) );
    }

    setValue( value ) {
        this._parent.setChecked( this.getAttribute( 'name' ), value, true );
    }
}
