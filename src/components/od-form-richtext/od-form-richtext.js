import OdFormElement from '../../lib/odFormElement.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        label.value-label::before {
            content: "";
            display: inherit;
            padding: .04em;
        }

    </style>
    
    <label for='input' id='label'>
        <slot id='label-slot'></slot>
    </label>
    <textarea id='richtext-textarea'>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-richtext" );

export class OdFormRichtext extends OdFormElement {
    constructor() {
        super( template );
    }

    _init() {
        this._labelSlot = this.shadowRoot.querySelector( '#label-slot' );
        this._input = this.shadowRoot.getElementById( "richtext-textarea" );
    }

    connectedCallback() {       
        super.connectedCallback();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["inline", "disabled", "hidden", "required"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "disabled": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setDisbaledAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setDisbaledAttribute( newValue );
                    if( this._parent && this._parent._setRichTextDisabled ) {
                        this._parent._setRichTextDisabled( this.getAttribute( 'name' ), newValue );
                    }
                }
                return;
            }
        }
        super.attributeChangedCallback( attrName, oldValue, newValue );
        if( newValue !== oldValue ) {
            this._updateAttributes();
        } 

    }

    _notifyParent() {
        this.dispatchEvent(
            new CustomEvent( 'od-form-richtext-get-parent', { 
                bubbles: true, 
                detail: { 
                    'odFormElement': this,
                    'label': this.labelText,
                    'callback': ( parent, editor ) => {
                        this._parent = parent;
                        this._editor = editor;                        
                        this._dispatchParentDone( this._parent );
                    } 
                }
            } )
        );
    } 
    
    _updateAttributes() { 
        let label = this.shadowRoot.querySelector( "label" );
        this.__updateAttributes( this.input, label );
    }

    _setDisabledAttribute( newV ) {
        this._setBooleanAttribute( 'disabled', newV );
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get editor() {
        return this._editor;
    }

    get value() {
        return this.getValue();
    }

    set value( value ) {
        this.setValue( value );
    }

    getValue() {
        if( !this._parent ) {
            return null;
        }
        return this._parent.getValue( this.getAttribute( 'name' ) );
    }

    setValue( value ) {
        this._parent.setValue( this.getAttribute( 'name' ), value );
    }
}
