import OdFormMultiselectElement from '../../lib/odFormMultiselectElement.js'

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        label.value-label::before {
            content: "";
            display: inherit;
            padding: .04em;
        }

    </style>
    
    <slot id='label-slot' name='label'></slot>
    <select id='select'></select>
    <slot id='select-slot'></slot>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-selectlist" );

export class OdFormSelectlist extends OdFormMultiselectElement {
    constructor() {
        super( template );
    }

    _init() {
        this._labelSlot = this.shadowRoot.querySelector( '#label-slot' );
        this._input = this.shadowRoot.getElementById( "select" );
    }

    connectedCallback() {
        super.connectedCallback();   
        this._upgradeSelectProperties();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    _upgradeSelectProperties() {
        let props = this._getSelectProperties();
        props.forEach( p => {
            this._upgradeProperty( p );
            this._setInputProperty( p ); 
        } );
    }

    _getSelectProperties() {
        var elemA = [];
        var inputA = [];
        for( var i in HTMLElement.prototype ) {
            elemA.push( i );
        }
        for ( var i in HTMLSelectElement.prototype ){
            if( !elemA.includes( i ) ) {
                inputA.push( i );
            }   
        }
        return inputA;
    }   

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["inline", "options-get", "options-value", "options-label", "disabled", "hidden", 
                "autofocus", "form", "multiple", "required", "size"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        if( newValue !== oldValue ) {
            this._updateAttributes();
        } 
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    
    //internal functions should begin with '_'    
    _updateAttributes() {
        let label = this.shadowRoot.querySelector( "#label-slot" ).assignedNodes()[0];
        this.__updateAttributes( this.input, label );
    }
    
    getValue() {
        if( !this._parent ) {
            return null;
        }
        return this._parent.getValue( this.getAttribute( 'name' ) );
    }

    setValue( value ) {
        this._parent.setValue( this.getAttribute( 'name' ), value );
    }
}
