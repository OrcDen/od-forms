import { OdFormTextArea } from "./od-form-textarea";
if ( !customElements.get( "od-form-textarea" ) ) {
    window.customElements.define( "od-form-textarea", OdFormTextArea );
}