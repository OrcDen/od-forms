import OdFormElement from '../../lib/odFormElement.js'

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        label.value-label::before {
            content: "";
            display: inherit;
            padding: .04em;
        }

    </style>
    
    <label for='input' id='label'>
        <slot id='label-slot'></slot>
    </label>
    <textarea id='textarea'>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form-textarea" );

export class OdFormTextArea extends OdFormElement {
    constructor() {
        super( template );
    }

    _init() {
        this._labelSlot = this.shadowRoot.querySelector( '#label-slot' );
        this._input = this.shadowRoot.getElementById( "textarea" );
    }

    connectedCallback() {       
        super.connectedCallback();
        this._upgradeTextareaProperties();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    _upgradeTextareaProperties() {
        let props = this._getTextareaProperties();
        props.forEach( p => {
            this._upgradeProperty( p );
            this._setInputProperty( p ); 
        } );
    }

    _getTextareaProperties() {
        var elemA = [];
        var inputA = [];
        for( var i in HTMLElement.prototype ) {
            elemA.push( i );
        }
        for ( var i in HTMLTextAreaElement.prototype ){
            if( !elemA.includes( i ) ) {
                inputA.push( i );
            }   
        }
        return inputA;
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["inline", "value", "disabled", "hidden", "autofocus", "cols", "dirname", "form", "maxlength", "placeholder", "readonly", 
                "required", "rows", "wrap"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        if( newValue !== oldValue ) {
            this._updateAttributes();
        }
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute

    //internal functions should begin with '_'
    _updateAttributes() { 
        let label = this.shadowRoot.querySelector( "label" );
        this.__updateAttributes( this.input, label );
    }
}
