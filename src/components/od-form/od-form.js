import FormBuilder from '../../lib/builders/formBuilder.js';
import CssBuilder from '../../lib/builders/cssBuilder.js';
import OptionsBuilder from '../../lib/builders/optionsBuilder.js';
import FormElementBuilder from '../../lib/builders/formElementBuilder.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        iframe {
            display: none;
        }

        form > div#table {
            display: flex;
            flex-direction: column;
            
            max-width: fit-content;
        }

        form > div#table > div.table-row {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        form > div#table > div.table-row > div.table-cell {
            display: flex;
        }

        form > div#table > div.table-row > div.label-cell {            
            flex-grow: 0;
            flex-shrink: 0;
            width: 15rem;
        }
        
        form label.field-label {
            font-size: 1em;

            padding: .2em .2em .2em .2em;
            margin: .2em .2em .2em .2em;

            border-left: 1px solid darkgrey;
            border-right: 1px solid darkgrey;
            border-top: 1px solid darkgrey;
            border-bottom: 1px solid darkgrey;

            background-color: #fff;
            color: #333;

            height: fit-content;
            width: fit-content;
        }

        form label.value-label {
            font-size:  1rem;

            padding: .3em .3em .3em .3em;
            margin: .3em .3em .3em .3em;

            background-color: #fff;
            color: #333;

            width: auto;
        }

        form input {
            font-size: 1rem;

            padding: .3em .3em .3em .3em;
            margin: .3em .3em .3em .3em;

            background-color: #fff;
            color: #333;

            height: fit-content;
            width: auto;
        }

        form select {
            font-size: 1rem;

            padding: .3em .3em .3em .3em;
            margin: .3em .3em .3em .3em;

            background-color: #fff;
            color: #333;

            height: fit-content;
            width: auto;
        }

        form[inline='true'] div#table{
            flex-direction: row;
        }

        form[disabled] input {
            display: none;
        }

        form[disabled] select {
            display: none;
        }

        form[disabled] textarea {
            display: none;
        }

        form[disabled] od-richtext {
            display: none;
        }

        form[disabled] fieldset > label.field-label {
            display: none;
        }

        form:not([disabled]) label.value-label {
            display: none !important;
        }
        
        form input#submit {
            display: none !important;
        }

        form input#reset {
            display: none !important;
        }
        
        #slot {
            display: none;
        }

        form textarea.richtext-textarea {
            display: none;
        }

        div.table-row[inline='true'] {
            flex-direction: row !important;
        }

        div.table-row[required='true'] div.label-cell::after {
            content: '*';
            color: red;
        }

        div.table-row[required='true'] legend::after {
            content: '*';
            color: red;
        }

        form *[show-group-required='true']::after {
            content: '\\00a0\\00a0\\00a0\\00a0Please Select at least one option.';
            color: red;
        }

        div.table-row[hidden='true'] {
            display: none;
        }

    </style>
    
    <iframe id="save-autocomplete" name="save-autocomplete" srcdoc="" ></iframe>
    <form id='form' part='form'>
        <div id='table' part='table'></div>
        <input type="submit" value="Submit" id='submit'>
        <input type="reset" value="Reset" id='reset'>    
    </form>
    <slot id='slot'></slot>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-form" );

export class OdForm extends HTMLElement {
    constructor() {
        super();
        this._ready = false;
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._form = this.shadowRoot.querySelector( "form" );
        let elementBuilder = new FormElementBuilder( this );
        this._formBuilder = new FormBuilder( this, new CssBuilder( this ), elementBuilder );
        this._optionsBuilder = new OptionsBuilder( this, elementBuilder );
        this._children = [];
        this._formElements = [];
        this._multiFormElements = [];
        this._options = [];

        //set attribute default values first and pass to element
        this._updateAttributes();

        //all properties should be upgraded to allow lazy functionality
        var props = this._getFormProperties();
        props.forEach( p => {
            this._upgradeProperty( p );
            if( p !== "elements" && p !== "length" ) {
                this._setFormProperty( p );                
            }
        } );
        this._upgradeProperty( 'disabled' );
        this._upgradeProperty( 'inline' );

        //listeners and others etc.
        this._childrenCount = this.shadowRoot.querySelector( '#slot' ).assignedNodes().filter( x => { return x.tagName && x.tagName.includes( 'OD-FORM-' ) } );

        this.form.addEventListener( "change", ( e ) => {
            this.dispatchEvent(
                new CustomEvent( 'od-form-change', { 
                    bubbles: true, 
                    detail: { 
                        'event': e,
                        'form': this,
                        'data': this.getData()
                    }
                } )
            );
        } );

        this.addEventListener( "od-form-element-get-parent", ( e ) => {
            this._getParentListener( e );
        } );
        this.addEventListener( "od-form-multiselect-get-parent", ( e ) => {
            this._getParentListenerMultiselect( e );
        } );
        this.addEventListener( "od-form-richtext-get-parent", ( e ) => {
            this._getParentListenerRichText( e );
        } );        
        this.addEventListener( "od-form-get-options", ( e ) => {
            this._getOptionsListener( e );
        } );
        this.addEventListener( "od-form-label-update", ( e ) => {
            this._updateLabel( e );
        } );

        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => { 
            this._mutationObserverCallback( mutationsList, observer ) 
        } );
        this._observer.observe( this.shadowRoot.querySelector( 'div#table' ), config );
    }

    disconnectedCallback() {
        //window listeners should always be disconnected since they are above our shadowRoot

        this._observer.disconnect();
    }   

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    _getFormProperties() {
        var elemA = [];
        var formA = [];
        for( var i in HTMLElement.prototype ) {
            elemA.push( i );
        }
        for ( var i in HTMLFormElement.prototype ){
            if( !elemA.includes( i ) ) {
                formA.push( i );    
            }   
        }
        return formA;
    }

    _setFormProperty( prop ) {
        if( this[prop] ){
            this.form[prop] = this[prop];
        }
    }

    _updateAttributes() {
        let form = this.shadowRoot.querySelector( "form" );
        let attributes = this.attributes;
        for( var i = 0; i < attributes.length; i++ ) {
            let attribute = attributes[i];
            if( attribute.name === 'part' || attribute.name === 'exportparts' ) {
                continue;
            }
            form.setAttribute( attribute.name, attribute.value );
        }
        let oldAttributes = form.attributes;
        for( var i = 0; i < oldAttributes.length; i++ ) {
            let attribute = oldAttributes[i];
            if( !attributes.getNamedItem( attribute.name ) && attribute.name !== 'part' && attribute.name !== 'exportparts' ) {
                form.removeAttribute( attribute.name );
            }            
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["disabled", "inline", "accept-charset", "action", "autocomplete", "enctype", "method", "name", "novalidate", "rel", "target"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "disabled": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setDisabledAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setDisabledAttribute( newValue );
                }
                break;
            }
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );
                }
                break;
            }
        }
        if( newValue !== oldValue ) {
            this._updateAttributes();
        }        
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////
    
    get form() {
        return this._form;
    }
    
    get ready() {
        return this._ready;
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get disabled() {
        return this.hasAttribute( "disabled" );
    }

    set disabled( isDisabled ) {
        if ( typeof isDisabled !== "boolean" ) {
            return;
        }
        this._setDisabledAttribute( isDisabled );
    }

    _setDisabledAttribute( newV ) {
        this._setBooleanAttribute( 'disabled', newV );
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
    }    

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    async _getParentListener( e ) {
        e.stopPropagation();

        let child = e.detail.odFormElement;
        let labelText = e.detail.labelText;

        if( child.tagName === 'OD-FORM-INPUT' && !this._isChild( child ) ) { //not parent, type is dynamically determined
            e.detail.callback( this );
            return;
        }
        this._children.push( { 'child': child, 'label': labelText } );
        await this._formBuilder.rebuildAttachElement( child, labelText );
        e.detail.callback( this );
        this._checkReadyStatus();
    }

    async _getParentListenerMultiselect( e ) {
        e.stopPropagation();

        let child = e.detail.odFormElement;
        let label = e.detail.label;
        let formElements = e.detail.formElements;  
        
        this._children.push( { 'child': child, 'label': label } );
        this._multiFormElements.push( { 'child': child, 'formElements': formElements } );        
        await this._formBuilder.rebuildAttachElement( child, label, formElements );
        e.detail.callback( this );
        this._checkReadyStatus();
    }

    async _getParentListenerRichText( e ) {
        e.stopPropagation();

        let child = e.detail.odFormElement;
        let labelText = e.detail.label;
        
        this._children.push( { 'child': child, 'label': labelText } );        
        let editor = await this._formBuilder.rebuildAttachElement( child, labelText );
        e.detail.callback( this, editor );
        this._checkReadyStatus();
    }

    //Options
    async _getOptionsListener( e ) {
        e.stopPropagation();
        if( this._isInsideArray() ) { //Is in an Array
            this.dispatchEvent(
                new CustomEvent( 'od-form-array-get-options', { 
                    bubbles: true, 
                    detail: { 
                        'deferred': e.detail,
                        'callback': ( url, options, id, type, name, required, labelField, valueField ) => {      
                            this._optionsBuilder.buildOptions( url, options, id, type, name, required, labelField, valueField );
                        }
                    }
                } )
            );
            return; //defer to array
        }
        let options = await this._optionsBuilder.getOptions( e.detail.url );
        this._optionsBuilder.buildOptions( e.detail.url, options, e.detail.id, e.detail.type, e.detail.name, e.detail.required, e.detail.labelField, e.detail.valueField );
    }

    //mutation Observer
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'od-form-table-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-form-table-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////
    _checkReadyStatus() {
        if( this._children.length === this._childrenCount.length ) {
            this._ready = true;
            this.dispatchEvent(
                new CustomEvent( 'od-form-ready', { 
                    bubbles: true, 
                    detail: { 
                        'ready': this.ready
                    }
                } )
            );
        }
    }

    _readyPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-form-ready', ( e ) => { this._readyPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    _isInsideArray() {
        return this.parentElement && this.parentElement.tagName === 'FIELDSET' && this.parentElement.id.includes( 'od-form-array-fieldset' )
    }

    _isChild( child ) {
        let children = this.shadowRoot.querySelector( '#slot' ).assignedElements()
        let found = false;
        for( let i = 0; i < children.length; i++ ) {
            let directChild = children[i];
            if( child === directChild ) {
                found = true;
            }
        }
        return found;
    }

    _dispatchElementUpdate( element ) { // refactor this out to a direct call eventually
        element.dispatchEvent(
            new CustomEvent( 'update-element', { 
                bubbles: true,
                detail: {
                    'id': element.id,
                    'value': element.value
                }
            } ) 
        ); 
    }

    _updateLabel( e ) {
        let formElement = e.detail.element;
        let newText = e.detail.newText;

        let labelName = formElement.getAttribute( 'name' );
        let label = this.form.querySelector( "label[for='" + labelName + "']" );
        if( label ) {
            label.innerHTML = newText;
        }
        label = this.form.querySelector( "label[id='" + labelName + "']" );
        if( label ) {
            label.innerHTML = newText;
        }
    }

    async _setField( e, value ) {
        if( value === null ) {
            return;
        }
        let check = e.getAttribute( 'options-get' );
        if( !this._isInsideArray() && check && !this._optionsBuilder.checkOptions( check ) ) {
            await this._optionsBuilder._optionsUpdatePromise( check );
        }
        let elemValue = e.value.toString();
        let dataValue = value.toString();
        if( e.tagName === 'SELECT' ) {
            this._setSelect( e, dataValue )
        } else if( e.tagName === 'OD_RICHTEXT' ) {
            e.dangerouslySetHTMLValue( value );
        } else if( e.type === 'radio' ) {
            if( elemValue && elemValue === dataValue ) {
                this._setChecked( e, true );
            }
        } else if ( e.type === 'checkbox' ) {
            if( value && value.includes && value.includes( elemValue ) ) {
                this._setChecked( e, true );
            }
        } else {
            e.value = value;
        }        
        this._dispatchElementUpdate( e );
    }

    _setChecked( e, isChecked ) {
        e.checked = isChecked;
    }

    _setSelect( e, dataValue ){
        let descendants = e.querySelectorAll( '*' );
        for( let i = 0; i < descendants.length; i++ ) {
            let c = descendants[i];
            let elemValue = c.value.toString();
            if( ( elemValue === dataValue ) || ( dataValue && dataValue.includes && dataValue.includes( elemValue ) ) ) { //to support optgroups and multiple select
                c.selected = 'selected';
            }
        }
    }

    _setFieldAttribute( id, attrName, attrValue ) {
        for( let i = 0; i < this._formElements.length; i++ ) {
            let elem = this._formElements[i];
            if( elem.id === id ) {
                if( attrValue || attrValue === "" ) {
                    elem.setAttribute( attrName, attrValue );
                } else {
                    elem.removeAttribute( attrName );
                }
                break;
            }
        }
    }
    
    async _setRequired( id, isRequired ) {
        let rows = this.shadowRoot.querySelectorAll( 'div.table-row#' + id );
        if( !rows || rows.length < 1 ) {
            await this.mutationPromise();
            rows = this.shadowRoot.querySelectorAll( 'div.table-row#' + id );
        }
        for( let i = 0; i < rows.length; i++ ) {
            if( isRequired === "" || isRequired === 'true' ) {
                rows[i].setAttribute( 'required', true );
            } else {
                rows[i].removeAttribute( 'required' );
            }
        }
    }
    
    async _setHidden( id, isHidden ) {
        let rows = this.shadowRoot.querySelectorAll( 'div.table-row#' + id );
        if( !rows || rows.length < 1 ) {
            await this.mutationPromise();
            rows = this.shadowRoot.querySelectorAll( 'div.table-row#' + id );
        }
        for( let i = 0; i < rows.length; i++ ) {
            if( isHidden === "" || isHidden === 'true' ) {
                rows[i].setAttribute( 'hidden', true );
            } else {
                rows[i].removeAttribute( 'hidden' );
            }
        }
    }

    _setGroupBooleanAttribute( fieldName, attrName, value ) {
        for( let i = 0; i < this._formElements.length; i++ ) {
            let elem = this._formElements[i];
            if( elem.getAttribute( 'name' ) === fieldName ) {
                if( value === "" || value === 'true' ) {
                    elem.setAttribute( attrName, true );
                } else {
                    elem.removeAttribute( attrName );
                }
            }
        }
    }

    _setRichTextDisabled( name, disabledString ) {
        let rtElem = this.shadowRoot.querySelector( "form od-richtext[name='" + name + "']" );
        rtElem.active = disabledString !== 'true';
    }

    async _addFieldListener( fieldId, eventName, callback ) {
        let fieldIds = this._formElements.map( ( x ) => { return x.id } );
        while( !fieldIds.includes( fieldId ) ) {
            await this.mutationPromise();
            fieldIds = this._formElements.map( ( x ) => { return x.id } );
        }
        for( let i = 0; i < this._formElements.length; i++ ) {
            let e = this._formElements[i];
            if( e.id === fieldId ) {                
                e.addEventListener( eventName, callback );
                break;       
            }
        }
    }

    async _addMultiFieldListener( fieldName, eventName, callback ) {
        let fieldNames = this._formElements.map( ( x ) => { return x.getAttribute( 'name' ) } );
        while( !fieldNames.includes( fieldName ) ) {
            await this.mutationPromise();
            fieldNames = this._formElements.map( ( x ) => { return x.getAttribute( 'name' ) } );
        }
        for( let i = 0; i < this._formElements.length; i++ ) {
            let e = this._formElements[i];
            if( e.getAttribute( 'name' ) === fieldName ) {         
                e.addEventListener( eventName, callback );     
            }
        }
    }

    _validateGroup( elem ) {
        let group = this._formElements.filter( e => {
                        return e.getAttribute( 'name' ) === elem.getAttribute( 'name' )
                    } );
        let valid = false;
        if( group.length > 1 ) {
            group.forEach( e => {
                if( e.checkValidity() ) {
                    valid = true;
                }
            } );
            this._showGroupRequired( elem, valid )
        } else {
            valid = elem.checkValidity();
            this._showRequired( elem, valid, "Required" )
        }
        return valid;
    }

    _showGroupRequired( element, valid ) {
        let parent = element.parentElement;
        if( !valid ) {
            parent.setAttribute( 'show-group-required', true );
        } else {
            parent.removeAttribute( 'show-group-required' );
        }
    }

    _showRequired( element, valid, message ) {
        let check = element.parentElement.querySelector( 'span#invalid' );
        if( check ) {
            element.parentElement.removeChild( check );
        }
        if( !valid ) {
            let messageSpan = document.createElement( 'span' );
            messageSpan.id = 'invalid';
            messageSpan.style.color = 'red';
            messageSpan.innerHTML = message
            element.parentElement.appendChild( messageSpan )
        }
    }

    _getInvalidMesssage( element ) {
        if( element.tagName === 'INPUT' ) {
            switch( element.getAttribute( 'type' ) ) {
                case 'email': {
                    return 'Format must be an email'
                }
                case 'number': {
                    return 'Format must be an number'
                }
                case 'tel': {
                    return 'Format must be a phone number'
                }
                case 'url': {
                    return 'Format must be a URL'
                }
                default : {
                    return 'Required';
                }
            }
        }
        if( element.tagName === 'SELECT' ) {
            return 'Please select an option.'
        }
        if( element.tagName === 'TEXTAREA' ) {
            return 'Required.'
        }        
    }

    _shadowSubmit() {
        //store the form attributes
        let saveTarget = this.form.getAttribute( 'target' );
        let saveMethod = this.form.getAttribute( 'method' );
        let saveAction = this.form.getAttribute( 'action' );

        //set and save the form data
        this.form.setAttribute( 'target', 'save-autocomplete' );
        this.form.setAttribute( 'method', 'post' );
        this.form.setAttribute( 'action', '' );
        this.htmlSubmitForm();

        //restore the form attributes
        if( saveTarget ) {
            this.form.setAttribute( 'target', saveTarget );
        }
        if( saveMethod ) {
            this.form.setAttribute( 'method', saveMethod );
        }
        if( saveAction ) {
            this.form.setAttribute( 'action', saveAction );
        }
    }

    _addMultiSelectOption( option, id, type, name, required, labelField, valueField ) {
        this._optionsBuilder.addOption( option, id, type, name, required, labelField, valueField );       
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////
    submitForm() {
        let saveNoValidate = this.form.getAttribute( 'novalidate' );

        this.form.setAttribute( 'novalidate', true );
        if( !this.validate() ) {
            return;
        }
        let submitButton = this.shadowRoot.querySelector( 'form input#submit' );
        submitButton.click();

        if( saveNoValidate ) {
            this.form.setAttribute( 'novalidate', saveNoValidate );
        } else {
            this.form.removeAttribute( 'novalidate' );
        }
    }

    htmlSubmitForm() {
        let submitButton = this.shadowRoot.querySelector( 'form input#submit' );
        submitButton.click();
    }

    resetForm() {
        let resetButton = this.shadowRoot.querySelector( 'form input#reset' );
        resetButton.click();        
    }

    validate() {
        let valid = true; // form valid
        this._formElements.forEach( ( elem ) => {
            let _valid = true; // elementvalid
            let type = elem.getAttribute( 'type' );
            if( type === 'checkbox' || type === 'radio' ) {
                if( elem.hasAttribute( 'required' ) && elem.getAttribute( 'required' ) === 'true' ) {                    
                    _valid = this._validateGroup( elem );
                    if( !_valid ) { // dont set form invalid unless element is invalid
                        valid = false; // form valid only ever gets set false
                    }
                }
            } else {
                _valid = elem.checkValidity( elem );
                if( !_valid ) { // dont set form invalid unless element is invalid
                    valid = false; // form valid only ever gets set false
                }
                this._showRequired( elem, _valid, this._getInvalidMesssage( elem ) );
            }
        } );
        return valid;
    }

    htmlValidate() {
        let saveNoValidate = this.form.getAttribute( 'novalidate' );
        let saveAutocomplete = this.form.getAttribute( 'autocomplete' );
        this.form.removeAttribute( 'novalidate' );
        this.form.setAttribute( 'autocomplete', 'off' );
        this._shadowSubmit();
        if( !saveAutocomplete ) {
            this.form.removeAttribute( 'autocomplete' );
        }
        if( saveNoValidate ) {
            this.form.setAttribute( 'autocomplete', saveNoValidate );
        }
    }

    getData() {
        let output = {}
        let data = new FormData( this.form );
        for( let pair of data.entries() ) {
            if( data.getAll( pair[0] ).length > 1 ) {
                output[pair[0]] = data.getAll( pair[0] );
            } else {
                output[pair[0]] = pair[1];
            }
        }
        return output;
    }

    async populate( data ) {
        this.resetForm();
        let parsed = JSON.parse( JSON.stringify( data ) );
        while( !this.ready ) {
            await this.readyPromise();
        }
        while( this._optionsBuilder.busy ) {
            await this.mutationPromise();
        }
        for( let i = 0; i < this._formElements.length; i++ ) {
            let e = this._formElements[i];
            if( Object.keys( parsed ).includes( e.name ) && e.type !== 'file' ) {                 
                await this._setField( e, parsed[e.name] )            
            }
        }
    }

    async setValue( name, value ) {
        while( this._formElements.length < 1 || this._optionsBuilder.busy ) {
            await this.mutationPromise();
        }
        this._formElements.forEach( async ( e ) => {
            if( e.getAttribute( 'name' ) === name ) {
                await this._setField( e, value );
            }
        } );
    }

    getValue( name ) {
        let data = this.getData();
        if( !data || Object.keys( data ).length < 1 ) {
            return null;
        }
        return data[name];
    }

    async setChecked( name, value, isChecked ) {
        while( this._formElements.length < 1 || this._optionsBuilder.busy ) {
            await this.mutationPromise();
        }
        this._formElements.forEach( ( e ) => {
            if( e.getAttribute( 'name' ) === name && ( e.type === 'checkbox' || e.type === 'radio' ) ) {
                let elemValue = e.value.toString();
                let dataValue = value.toString();
                if( elemValue === dataValue || ( dataValue.includes && dataValue.includes( elemValue ) ) ) {
                    if( !isChecked || isChecked === 'false' ) {
                        this._setChecked( e, false );
                    } else {
                        this._setChecked( e, true );
                    }
                }     
                this._dispatchElementUpdate( e );
            }
        } );        
    }

    async setInline( name, isInline ) {
        let found = null;
        for( let i = 0; i < this._formElements.length; i++ ) {
            let fElem = this._formElements[i];
            if( fElem.getAttribute( 'name' ) !== name ) {
                continue;
            }
            found = fElem;
        }
        if( !found ) {
            return;
        }
        let row = found.parentElement.parentElement;
        if( isInline === 'true' || isInline === '' ) {
            row.setAttribute( 'inline', true );
        } else {
            row.removeAttribute( 'inline' )
        }
    }

    saveAutocomplete() {
        let saveNoValidate = this.form.getAttribute( 'novalidate' );
        let saveAutocomplete = this.form.getAttribute( 'autocomplete' );
        this.form.setAttribute( 'novalidate', 'true' );
        this.form.removeAttribute( 'autocomplete' );
        this._shadowSubmit();
        if( !saveNoValidate ) {
            this.form.removeAttribute( 'novalidate' );
        }
        if( saveAutocomplete ) {
            this.form.setAttribute( 'autocomplete', saveAutocomplete );
        }
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-form-table-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }
    
    async readyPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-form-ready', ( e ) => { this._readyPromiseCallback( e, resolve ); } );
        } );
    }
}
