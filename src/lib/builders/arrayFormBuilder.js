export default class ArrayFormBuilder {

    constructor( scope ) {
        this._scope = scope;
    }

    async buildTemplate( template, templateItems ) {
        let form = document.createElement( 'od-form' );
        form.id = this._scope.id + this._generateId();
        form.setAttribute( 'part', 'od-form' );
        form.setAttribute( 'exportparts', 'form, table, table-row, table-cell, label-cell, input-cell, field-label, field-label-row, value-label ,form-input, form-select, form-textarea, form-richtext, richtext-editor' );
        let inline = this._scope.inline;
        form.inline = inline;
        for( var i = 0; i < templateItems.length; i++ ) {
            let child = templateItems[i];
            form.appendChild( child );
        }
        let fieldset = document.createElement( 'fieldset' );  
        fieldset.setAttribute( 'part', 'array-fieldset' );      
        fieldset.appendChild( form )
        template.appendChild( fieldset );
    }

    async addNewEntry( template ) {
        let clone = template.firstChild.cloneNode( true );
        let newId = this._generateId()
        let form = clone.firstChild;
        clone.id = 'od-form-array-fieldset' + newId;
        clone.appendChild( this._buildDeleteButton( 'od-form-array-fieldset-delete' + newId ) );
        form.id += newId;
        form.addEventListener( "od-form-array-get-options", ( e ) => {
            this._scope._getOptionsListener( e );
        } );
        this._scope._container.appendChild( clone );
        await this._scope.mutationPromise();
        return form;
    }

    async deleteEntry( id ) {
        let deleteMe = null;
        let fieldsetIdPrefix = 'od-form-array-fieldset';
        let deleteIdPrefix = 'od-form-array-fieldset-delete';
        this._scope.forms.forEach( ( form ) => {
            let fieldset = form.parentElement;
            if( fieldset.id.substring( fieldsetIdPrefix.length ) === id.substring( deleteIdPrefix.length )
                || fieldset.id === id ) {
                deleteMe = form;
            }
        } );
        this._scope._container.removeChild( deleteMe.parentElement );
        await this._scope.mutationPromise();
        return deleteMe;
    }

    _generateId() {
        if( !this.__tempId ) {
            this.__tempId = 0;
        }
        return this.__tempId++;
    }

    _buildDeleteButton( id ) {
        let button = document.createElement( 'button' );
        button.classList.add( 'delete-button' );
        button.id = id;
        button.setAttribute( 'part', 'delete-button' );
        button.onclick = ( e ) => { this._scope.deleteEntry( e.target.id ); };
        return button;
    }
}