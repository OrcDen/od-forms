export default class CssBuilder {

    constructor( scope ) {
        this._scope = scope;
    }

    rebuildNodeStyles( element ) {
        let parentForm = this._scope.form;
        let shadow = element.shadowRoot;
        let shadowStyles = shadow.styleSheets;
        for( var i = 0; i < shadowStyles.length; i++ ) {
            let sheet = shadowStyles[i];
            let styleRules = sheet.cssRules;
            for( var j = 0; j < styleRules.length; j++ ) {
                let rule = styleRules[j];
                let styles = rule.style;
                this._addNodeStyles( rule.selectorText, styles, parentForm );
            }
        }
    }

    _addNodeStyles( selector, styles, parentForm ) {
        let formId = parentForm.id;
        let styleElem = parentForm.querySelector( 'style' );
        if( !styleElem ) {
            styleElem = document.createElement( 'style' );
            styleElem.setAttribute( 'scoped', true );
            styleElem.setAttribute( 'type', 'text/css' );
            parentForm.appendChild( styleElem );
        }
        this._addStyleSelectorBlock( styleElem, formId, selector, styles );  
    }

    _addStyleSelectorBlock( styleElem, formId, selector, styles ) {
        let newline = `
`;
        let index = 0;
        let formSelector = 'form#' + formId + ' ' + selector + ' {';
        let selectIndex = styleElem.innerHTML.indexOf( formSelector );
        let closeIndex = styleElem.innerHTML.indexOf( '}', formSelector );
        let check = styleElem.innerHTML.substring( selectIndex, closeIndex );

        if( styleElem.innerHTML.includes( formSelector ) ) {
            //remove '}'
            index = closeIndex;
            let begining = styleElem.innerHTML.substring( 0, index );
            let end = styleElem.innerHTML.substring( index + 1 );
            styleElem.innerHTML = begining + end;
            index -= 1;
        } else {
            //add new selector
            styleElem.innerHTML += newline + formSelector;
            index = styleElem.innerHTML.length - 1;
        }
        for( var k = 0; k < styles.length; k++ ) {
            let style = styles[k];
            let styleString = null;
            //these are common but have nested patterns - get the original declaration            
            let first = style.indexOf( '-' );
            if( style.includes( 'border' ) ) {
                let second = style.indexOf( '-', first + 1 )
                style = style.substring( 0, second );
            } else if( style.includes( 'margin' ) || style.includes( 'padding' ) ) {
                style = style.substring( 0, first );
            }

            styleString = '    ' + style + ': ' + styles.getPropertyValue( style ) + ';'
            //check if exists
            if( check.includes( styleString ) ) {
                continue;
            }

            //add new style
            let newInner = styleElem.innerHTML.substring( 0, index );
            newInner += newline + styleString;           
            styleElem.innerHTML = newInner;
            index = newInner.length - 1;
        }
        //finish by adding '}' back
        let final = styleElem.innerHTML.substring( index, styleElem.innerHTML.length );
        styleElem.innerHTML += newline + '}' + newline + final;
    }
}