export default class FormBuilder {

    constructor( scope, cssBuilder, elementBuilder ) {
        this._scope = scope;
        this._cssBuilder = cssBuilder;
        this._elementBuilder = elementBuilder;
    }

    async rebuildAttachElement( element, labelText, formElements, noStyle ) {
        if( !formElements || !labelText ) {
            if( element.tagName === 'OD-FORM-SELECTLIST' ) {
                let output = await this.rebuildAttachFormElement( element, this._cloneSelectElementNode, noStyle );
                element.getMultiSelectOptions( element.tagName );
                return output;
            } else if( element.tagName === 'OD-FORM-RICHTEXT' ) {
                return await this.rebuildAttachRichtextFormElement( element, noStyle );
            } else {
                return await this.rebuildAttachFormElement( element, this._cloneElementNode, noStyle );//normal
            }
        } else {
            let output = await this.rebuildAttachMultiselectElement( element, labelText, formElements, noStyle );
            element.getMultiSelectOptions( element.tagName );
            return output;
        }
    }

    async rebuildAttachFormElement( element, cloneElementFunction, noStyle ) {        
        let row = await this._buildRow( element );

        let nodes = null;
        nodes = this._rebuildFormElement( element, cloneElementFunction );

        this._attachCells( element, row, nodes, noStyle );

        await this.mutationPromise();
        return nodes.input;
    }

    async rebuildAttachRichtextFormElement( element, noStyle ) {   
        let row = await this._buildRow( element );

        let nodes = null;
        nodes = this._rebuildFormRichtextElement( element );

        this._attachCells( element, row, nodes, noStyle )

        await this.mutationPromise();
        return nodes.input;
    }

    async rebuildAttachMultiselectElement( element, labelText, formElements, noStyle ) {
        let row = await this._buildRow( element );   

        let cell = document.createElement( 'div' );
        cell.classList.add( 'table-cell' );

        row.appendChild( cell );
        if( element.getAttribute( 'inline' ) === 'true' ) {
            row.setAttribute( 'inline', true );
        }
        let fieldset = document.createElement( 'fieldset' );
        fieldset.classList.add( 'multiselect-fieldset' );
        fieldset.setAttribute( 'part', 'multiselect-fieldset' );
        let legend = document.createElement( 'legend' );
        legend.classList.add( 'multiselect-legend' );
        legend.setAttribute( 'part', 'multiselect-legend' );
        legend.innerHTML = labelText;
        fieldset.appendChild( legend );

        cell.appendChild( fieldset );
        for( var i = 0; i < formElements.length; i++ ) {
            let checkbox = formElements[i];
            let nodes = this._rebuildFormElement( checkbox, this._cloneElementNode );
            if( element.tagName === 'OD-FORM-MULTISELECT' ) {
                nodes.input.setAttribute( 'type', 'checkbox' );
            } else {
                nodes.input.setAttribute( 'type', 'radio' );
            }
            nodes.input.id = checkbox.value;
            nodes.input.setAttribute( 'name', element.getAttribute( 'name' ) );
            nodes.input.setAttribute( 'options-get', element.getAttribute( 'options-get' ) );
            if( element.getAttribute( 'required' ) === 'true' ) {
                nodes.input.setAttribute( 'required', true );
            }
            if( element.getAttribute( 'disabled' ) === 'true' ) {
                nodes.input.setAttribute( 'disabled', true );
            }
            if( nodes.label ) {
                fieldset.appendChild( nodes.label );
            } 
            fieldset.appendChild( this._elementBuilder.buildValueLabel( checkbox.value, element.getAttribute( 'name' ) ) );  
            fieldset.appendChild( nodes.input );        
        }

        if( !noStyle ) {
            this._cssBuilder.rebuildNodeStyles( element );
        }        
        await this.mutationPromise();
    }

    _rebuildFormElement( element, cloneElementFunction ) {
        let output = { 'label': null, 'input': null };

        let clone = null;
        let shadow = element.shadowRoot;
        let nodes = shadow.querySelectorAll( '*' );

        for( var i = 0; i < nodes.length; i++ ) {
            let node = nodes[i];
            if( node.tagName === 'STYLE' ) {
                continue;
            }
            if( node.tagName === 'INPUT' || node.tagName === 'TEXTAREA' ) {
                clone = cloneElementFunction( this._scope, this, node );
                if( node.tagName === 'INPUT' ) {
                    clone.setAttribute( 'part', 'form-input' );
                } else {
                    clone.setAttribute( 'part', 'form-textarea' );
                }
                clone.setAttribute( 'name', element.getAttribute( 'name' ) );
                clone.id = element.getAttribute( 'name' );
                output.input = clone;
            } else if ( node.tagName === 'SELECT' ){
                let options = element.shadowRoot.querySelector( '#select-slot' ).assignedNodes( {flatten: true} );
                clone = cloneElementFunction( this._scope, this, node, options );
                clone.setAttribute( 'part', 'form-select' );
                clone.setAttribute( 'name', element.getAttribute( 'name' ) );
                if( element.getAttribute( 'options-get' ) ) {
                    clone.setAttribute( 'options-get', element.getAttribute( 'options-get' ) );
                }
                clone.id = element.getAttribute( 'name' );
                output.input = clone;
            } else if( node.tagName === 'LABEL' || ( element.tagName === 'OD-FORM-SELECTLIST' && node.id === 'label-slot' ) ) {
                clone = this._elementBuilder.buildFieldLabel( element.getAttribute( 'name' ), element.labelText );
                output.label = clone;
            }
        }
        return output;
    }
    
    _rebuildFormRichtextElement( element ) {
        let output = { 'label': null, 'input': null };

        let inputDiv = document.createElement( 'div' );
        inputDiv.style.display = 'contents';
        output.input = inputDiv;

        let rtElem = document.createElement( 'od-richtext' );
        rtElem.id = element.getAttribute( 'name' );
        rtElem.setAttribute( 'part', 'form-richtext' );
        rtElem.setAttribute( 'exportparts', 'richtext-editor' );
        inputDiv.appendChild( rtElem );
        
        let clone = null;
        let shadow = element.shadowRoot;
        let nodes = shadow.querySelectorAll( '*' );
        for( var i = 0; i < nodes.length; i++ ) {
            let node = nodes[i];
            if( node.tagName === 'STYLE' ) {
                continue;
            }
            if( node.tagName === 'TEXTAREA' ) {
                clone = node.cloneNode( true ); 
                this._elementBuilder.addRichtextListeners( rtElem, clone );
                this._elementBuilder.addRTTextareaListeners( clone, rtElem );
                clone.setAttribute( 'name', element.getAttribute( 'name' ) );
                clone.classList.add( 'richtext-textarea' );
                clone.id = element.getAttribute( 'name' );
                inputDiv.appendChild( clone );
                this._scope._formElements.push( clone );
            } else if( node.tagName === 'LABEL' ) {
                clone = this._elementBuilder.buildFieldLabel( element.getAttribute( 'name' ), element.labelText );
                output.label = clone;
            }
        }        
        return output;
    }

    _attachCells( element, row, nodes, noStyle ) {
        
        let cell = document.createElement( 'div' );
        cell.classList.add( 'table-cell' );
        cell.setAttribute( 'part', 'table-cell' );
        let cell2 = document.createElement( 'div' );
        cell2.setAttribute( 'part', 'table-cell input-cell' );
        cell2.classList.add( 'table-cell' );

        if( nodes.label ) {
            cell.setAttribute( 'part', 'table-cell label-cell' );
            cell.classList.add( 'label-cell' )
            cell.appendChild( nodes.label );
        } 
        cell2.appendChild( this._elementBuilder.buildValueLabel( element.getAttribute( 'name' ), element.getAttribute( 'name' ) ) );
        cell2.appendChild( nodes.input );

        row.appendChild( cell );  
        row.appendChild( cell2 );
        if( element.getAttribute( 'inline' ) === 'true' ) {
            row.setAttribute( 'inline', true );
        }
        if( !noStyle ) {
            this._cssBuilder.rebuildNodeStyles( element );
        }
    }

    _cloneElementNode( formScope, scope, node ) {
        let clone = node.cloneNode( true ); 
        scope._elementBuilder.addListeners( clone );
        formScope._formElements.push( clone );
        return clone
    }

    _cloneSelectElementNode( formScope, scope, node, options ) {
        let clone = node.cloneNode( true );
        for( let i = 0; i < options.length; i++ ) {
            let option = options[i];
            clone.appendChild( option.cloneNode( true ) )
        }
        scope._elementBuilder.addListeners( clone );
        formScope._formElements.push( clone );
        return clone
    }

    async _buildRow( element ) {
        let row = document.createElement( 'div' );
        row.id = element.getAttribute( 'name' );
        row.classList.add( 'table-row' );
        row.setAttribute( 'part', 'table-row' );        
        this._setRequired( element, row );
        await this._findAppendRow( element, row );
        return row;
    }

    async _findAppendRow( element, row ) {
        let formTable = this._scope.shadowRoot.querySelector( 'div#table' );
        let nextElem = null;
        if( element.nextElementSibling ) {
            nextElem = this._scope.shadowRoot.querySelector( 'form div#table div.table-row#' + element.nextElementSibling.getAttribute( 'name' ) );
        }
        if( !element.previousElementSibling ) { //first in the table
            formTable.insertBefore( row, formTable.firstChild )
        } else if( !element.nextElementSibling ) {//last in the table
            formTable.appendChild( row );
        } else if( nextElem ) { //Theres a subsequent element to insert before
            formTable.insertBefore( row, nextElem )
        } else { //loop the dom and try to find a next element
            let next = element.nextElementSibling
            let query = this._scope.shadowRoot.querySelector( 'form div#table div.table-row#' + next.getAttribute( 'name' ) );            
            while( next !== null && !query ) {
                next = next.nextElementSibling;
                if( next ) {
                    query = this._scope.shadowRoot.querySelector( 'form div#table div.table-row#' + next.getAttribute( 'name' ) )
                } else {
                    query = null;
                }
            }
            if( query ) { // subsequent elemnt found
                formTable.insertBefore( row, query )
            } else {
                formTable.appendChild( row ); //
            }
        }
        await this.mutationPromise();
    }

    _setRequired( element, row ) {
        let isRequired = element.getAttribute( 'required' ) === 'true';
        if( isRequired ) {
            row.setAttribute( 'required', true );
        }
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this._scope.addEventListener( 'od-form-table-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        } );
    }

    _mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this._scope.removeEventListener( 'od-form-table-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }
}