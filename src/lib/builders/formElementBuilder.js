export default class FormElementBuilder {

    constructor( scope ) {
        this._scope = scope;
    }

    buildFieldLabel( id, text ) {
        if( !text ) {
            return;
        }
        let newLabel = document.createElement( 'label' );
        newLabel.innerHTML = text
        newLabel.classList.add( 'field-label' );
        newLabel.setAttribute( 'for', id );
        newLabel.setAttribute( 'part', 'field-label' );
        return newLabel
    }

    buildValueLabel( id, name ) {
        let newLabel = document.createElement( 'label' );
        newLabel.id = id;
        newLabel.classList.add( 'value-label' );
        newLabel.setAttribute( 'name', name );
        newLabel.setAttribute( 'part', 'value-label' );
        return newLabel
    }

    buildInput( id, type, value, labelText, name, required ){
        let newOption = document.createElement( 'input' );
        newOption.id = id;
        newOption.type = type;
        newOption.value = value;
        newOption.setAttribute( 'name', name );
        newOption.setAttribute( 'required', required );
        newOption.setAttribute( 'part', 'form-input' );
        this.addListeners( newOption );
        if( type === 'radio' || type === 'checkbox' ) {
            newOption.setAttribute( 'label-text', labelText );
        }
        return newOption;
    }

    updateElement( e, id, value ) {
        this._updateValueLabel( e, id, value );
        this._updateValueCheckedAttributes( e, id, value );
    }

    _updateValueCheckedAttributes( e, id, value ) {
        let elems = this._scope.shadowRoot.querySelector( '#slot' ).assignedNodes();
        for( let i = 0; i < elems.length; i++ ) {
            let elem = elems[i];
            if( elem && elem.getAttribute && elem.getAttribute( 'name' ) === id ) {
                if( !value ) {
                    elem.removeAttribute( 'value' );
                } else {
                    elem.setAttribute( 'value', value );
                }
                break;
            } else if( elem.tagName === 'OD-FORM-MULTISELECT' || elem.tagName === 'OD-FORM-RADIOSELECT' ) {
                let found = false;
                let children = elem.shadowRoot.querySelector( '#input-slot' ).assignedNodes();
                for( let j = 0; j < children.length; j++ ) {
                    let child = children[j];
                    if( child.value && child.value === value ) {
                        found = true;
                        if( e.target.checked ) {
                            child.setAttribute( 'checked', true );
                        } else {
                            child.removeAttribute( 'checked' )
                        }
                    }
                }
                if( found ) {
                    break;
                }
            }
        }
    }

    _updateValueLabel( e, id, value ) {
        let labels = this._scope.shadowRoot.querySelectorAll( 'form div#table div.table-row label' );
        let label = null;
        for( let i = 0; i < labels.length; i++ ) { //cant query the label directly for some reason
            let l = labels[i];
            if( l.id === id ) {
                label = l;
            }
        }
        if( label && e.target.getAttribute( 'type' ) === 'checkbox' && !e.target.checked ) {
            label.innerHTML = '';
        } else if( e.target.getAttribute( 'type' ) === 'checkbox' ) {            
            this._updpateMultiValueLabel( e, label, value );
        } else if( e.target.getAttribute( 'type' ) === 'radio' ) {         
            this._updateRadioValueLabel( e, id, value );
        } else if( e.target.tagName === 'SELECT' ) {
            this._updateSelectValueLabel( e, label );
        } 
        else if ( label ) {
            label.innerHTML = value;
        }
    }

    addListeners( input ) {
        input.addEventListener( 'change', ( e ) => {
            this.updateElement( e, e.target.id, e.target.value );
        } );
        input.addEventListener( 'update-element', ( e ) => {
            e.stopPropagation();
            this.updateElement( e, e.detail.id, e.detail.value );
        } );
    }

    addRichtextListeners( richtext, textarea ) {
        richtext.addEventListener( 'richtext-change', ( e ) => {
            let styles = this._parseRTStyles( e.detail.styleTag.outerHTML );
            textarea.value = styles + e.detail.value;
            this._updateValueLabel( e, textarea.id, richtext.text );
        } );
    }

    addRTTextareaListeners( textarea, richtext ) {
        textarea.addEventListener( 'change', ( e ) => {
            let newValue = this._removeRTStyles( e.target.value )
            richtext.dangerouslySetHTMLValue( newValue );
        } );
        textarea.addEventListener( 'update-element', ( e ) => {
            e.stopPropagation();
            let newValue = this._removeRTStyles( e.detail.value )
            richtext.dangerouslySetHTMLValue( newValue );
        } );
    }

    _removeRTStyles( value ) {
        return value.substring( value.indexOf( '</style>' ) + 8 );
    }

    _parseRTStyles( styles ) {
        //console.log( styles )
        let newStyles = styles.substring( 0, styles.indexOf( '.ql-container {' ) ) 
                        + styles.substring( styles.indexOf( '.ql-editor p,' ), styles.indexOf( '.ql-editor.ql-blank::before {' ) )
                        + styles.substring( styles.indexOf( '</style>' ), styles.length );
        newStyles = newStyles.replaceAll( '.ql-editor ', '' );
        //console.log( newStyles )
        return newStyles;
    }

    _updateRadioValueLabel( e, id, value ) {
        let allLabels = this._scope.shadowRoot.querySelectorAll( 'form label.value-label[name=' + e.target.getAttribute( 'name' ) +']' );
        for( let i = 0; i < allLabels.length; i++ ) {
            let label = allLabels[i];
            if( label.id === id ) {
                this._updpateMultiValueLabel( e, label, value )
            } else {
                label.innerHTML = '';
            }
        }
    }

    _updpateMultiValueLabel( e, label, value ) {
        let labeltext = e.target.getAttribute( 'label-text' );
        if( labeltext ) {
            label.innerHTML = labeltext;
        } else {
            label.innerHTML = value;
        }
    }

    _updateSelectValueLabel( e, label ) {
        let select = e.target;
        let index = select.selectedIndex;
        label.innerHTML = select.options[index].innerHTML;
    }
}