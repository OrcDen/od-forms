export default class OptionsBuilder {
    
    constructor( scope, elementBuilder ) {
        this._scope = scope;
        this._elementBuilder = elementBuilder;

        this._busy = false;
    }

    get busy() {
        return this._busy;
    }

    set busy( busy ) {
        this._busy = busy;
    }

    async getOptions( url ) {
        let optionsCheck = this._checkOptions( url ); //check dynamic options object for a call in progress or completed
        if( optionsCheck ) { //if we have options already dont make another call and return them
            if( optionsCheck.options ) { 
                return optionsCheck.options;
            } else { //we started the call but it hasn't finished
                let options = await this._optionsUpdatePromise( url ); //not done getting options
                return options;
            }
        }
        return await this._getOptions( url );
    }

    checkOptions( url ) {
        let optionsCheck = this._checkOptions( url );
        if( optionsCheck ) { 
            if( optionsCheck.options ) { 
                return true;
            }
        }
        return false;
    }

    async buildOptions( url, options, id, type, name, required, labelField, valueField ) {
        let element = await this._findElement( id );
        if( type === 'select' ) {
            this._buildSelectOptions( element, options, labelField, valueField );
        } else {
            this._buildMultiOptions( element, options, labelField, valueField, type, name, required );
        }        
        this.checkUpdateBusy();
        this._dispatchOptionsUpdate( url, options );
    }

    checkUpdateBusy() {
        let busy = false;
        this._scope._options.forEach( ( opt ) => {
            if( !opt.options ) {
                busy = true;
            }
        } );
        this.busy = busy;
    }

    async addOption( option, id, type, name, required, labelField, valueField ) {
        let element = await this._findElement( id );
        if( type === 'select' ) {
            this._buildSelectOption( element, option, labelField, valueField );
        } else {
            this._buildMultiOption( element, option, labelField, valueField, type, name, required );
        }
    }

    async _findElement( id ) {
        let rows = this._scope.shadowRoot.querySelector( 'div#table' ).querySelectorAll( 'div.table-row' );
        let found = this.__findElement( rows, id );
        while( !found ) {
            await this._scope.mutationPromise();
            rows = rows = this._scope.shadowRoot.querySelector( 'div#table' ).querySelectorAll( 'div.table-row' );
            found = this.__findElement( rows, id );
        }
        return found;
    }

    __findElement( rows, id ) {
        let found = null;
        for( let r of rows ) { //iterator
            if( r.id === id ) {
                let cells = r.querySelectorAll( 'div.table-cell' );
                for( let c of cells ) { //iterator
                    let elements = c.childNodes;
                    for( let elem of elements ) {
                        if( elem.tagName !== 'LABEL' ) {
                            found = elem;
                            break;
                        }
                    }
                }
            }
        }
        return found;
    }

    _buildSelectOptions( select, options, labelField, valueField ) {
        for( let i = 0; i < options.length; i++ ) {
            let option = options[i];
            this._buildSelectOption( select, option, labelField, valueField );
        }
    }

    _buildSelectOption( select, option, labelField, valueField ) {
        let found = false;
        for( let i = 0; i < select.childNodes.length; i++ ) {
            let child = select.childNodes[i];
            if( child.value === option[valueField].toString() ) {
                found = true;
                break;
            }
        }
        if( found ) {
            return;
        }
        let newOption = document.createElement( 'option' );
        newOption.value = option[valueField];
        newOption.innerHTML = option[labelField];
        select.appendChild( newOption );
    }

    _buildMultiOptions( fieldset, options, labelField, valueField, type, name, required ) {
        for( let i = 0; i < options.length; i++ ) {
            let option = options[i];
            this._buildMultiOption( fieldset, option, labelField, valueField, type, name, required );
        }
    }

    _buildMultiOption( fieldset, option, labelField, valueField, type, name, required ) {
        let found = false;
        for( let i = 0; i < fieldset.childNodes.length; i++ ) {
            let child = fieldset.childNodes[i];
            if( child.value === option[valueField].toString() ) {
                found = true;
                break;
            }
        }
        if( found ) {
            return;
        }
        let newId = 'od-form-generated-option' + this._generateId();
        let input = this._elementBuilder.buildInput( newId, type, option[valueField], option[labelField], name, required );
        let optionContainer = document.createElement( 'div' );
        optionContainer.classList.add( 'option-container' );
        optionContainer.setAttribute( 'part', 'option-container' );
        optionContainer.appendChild( input );
        optionContainer.appendChild( this._elementBuilder.buildFieldLabel( newId, option[labelField] ) );
        optionContainer.appendChild( this._elementBuilder.buildValueLabel( newId, name ) );
        fieldset.appendChild( optionContainer );
        this._scope._formElements.push( input );
    }    

    async _getOptions( url ) {
        this.busy = true;
        this._storeTempOptions( url );
        this._httpGetAsync( url );
        let response = await this._httpPromise( url );
        this._updateOptions( url, response );
        return response;
    }

    _checkOptions( url ) {
        let found = null;
        this._scope._options.forEach( ( opt ) => {
            if( opt.url === url ) {
                found = opt;
            }
        } );
        return found;
    }

    _updateOptions( url, options ) {
        this._scope._options.forEach( ( opt ) => {
            if( opt.url === url ) {
                opt.options = options;
            } 
        } );
    }

    _storeTempOptions( url ) {
        if( this._checkOptions( url ) ) {
            return;
        }
        this._scope._options.push( { 'url': url, 'options': null } );
    }

    _httpGetAsync( theUrl ) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = () => { 
        if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )
            this._scope.dispatchEvent(
                new CustomEvent( 'od-form-request-done', { 
                    bubbles: true, 
                    detail: {
                        'url': theUrl, 
                        'response': JSON.parse( xmlHttp.responseText )
                    }
                } )
            );
        }
        xmlHttp.open( "GET", theUrl, true ); // true for asynchronous 
        xmlHttp.send( null );
    }

    async _httpPromise( url ) {
        return new Promise( ( resolve ) => { 
            this._scope.addEventListener( 'od-form-request-done', ( e ) => { this._httpPromiseCallback( url, e, resolve ); } );
        } );
    }

    _httpPromiseCallback( url, e, resolve ) {
        if( url !== e.detail.url ) { //this must match or we return the wrong data form the http request
            return;
        }
        e.stopPropagation();
        this._scope.removeEventListener( 'od-form-request-done', ( e ) => { this._httpPromiseCallback( url, e, resolve ); } );
        resolve( e.detail.response ); 
    } 

    _dispatchOptionsUpdate( theUrl, options ) {
        this._scope.dispatchEvent(
            new CustomEvent( 'od-form-options-update', { 
                bubbles: true, 
                detail: {
                    'url': theUrl, 
                    'options': options
                }
            } )
        );
    }

    async _optionsUpdatePromise( url ) {
        return new Promise( ( resolve ) => { 
            this._scope.addEventListener( 'od-form-options-update', ( e ) => { this._optionsPromiseCallback( url, e, resolve ); } );
        } );
    }

    _optionsPromiseCallback( url, e, resolve ) {
        if( url !== e.detail.url ) {
            return;
        }
        this._scope.removeEventListener( 'od-form-options-update', ( e ) => { this._optionsPromiseCallback( url, e, resolve ); } );
        resolve( e.detail.options ); 
    }
    
    _generateId() {
        if( !this.__tempId ) {
            this.__tempId = 0;
        }
        return this.__tempId++;
    }
}