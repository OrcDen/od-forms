export default class OdFormElement extends HTMLElement {

    constructor( template ) {
        super();
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        this._init();

        this._labelText = this._getLabelText();
        
        this._notifyParent();

        this._updateAttributes();
        this._upgradeProperty( "inline" );
        this._upgradeProperty( "checked" );
        this._upgradeProperty( "value" );
       
        let config = { attributes: false, childList: true, subtree: true, characterData: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {
            this.labelText = this._getLabelText();
        } );
        this._observer.observe( this, config );
    }

    disconnectedCallback() {
        return;
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );

                    this._parentSetInline( newValue );          
                }
                break;                
            }
            case "checked": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setCheckedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setCheckedAttribute( newValue );

                    this._parentSetChecked( newValue );                     
                }
                break;                
            }
            case "required": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setRequiredAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setRequiredAttribute( newValue );

                    this._parentSetRequired( newValue );
                    this._parentSetFieldAttribute( attrName, newValue );
                }
                break;                
            }
            case "hidden": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setHiddenAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setHiddenAttribute( newValue );

                    this._parentSetHidden( newValue );
                    this._parentSetFieldAttribute( attrName, newValue );
                }
                break;                
            }
            case "value": {
                if( newValue !== oldValue ) {
                    this._parentSetValue( newValue );          
                }
                break;                
            }
            default: {
                if( newValue !== oldValue ) { 
                    this._parentSetFieldAttribute( attrName, newValue );           
                }
                break;
            }
        }
    }

    _upgradeInputProperties() {
        let props = this._getInputProperties();
        props.forEach( p => {
            this._upgradeProperty( p );
            this._setInputProperty( p );  
        } );
    }

    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    _addAttribute( input, label, attribute ) {
        input.setAttribute( attribute.name, attribute.value );
        if( attribute.name === 'inline' ) {                
            label.setAttribute( attribute.name, attribute.value );
        }
    }

    _removeAttribute( input, label, attribute ) {
        input.removeAttribute( attribute.name );
        if( attribute.name === 'inline' ) {                
            label.removeAttribute( attribute.name );
        }
    }

    _getInputProperties() {
        var elemA = [];
        var inputA = [];
        for( var i in HTMLElement.prototype ) {
            elemA.push( i );
        }
        for ( var i in HTMLInputElement.prototype ){
            if( !elemA.includes( i ) ) {
                inputA.push( i );
            }   
        }
        return inputA;
    }

    _setInputProperty( prop ) {
        if( this.input && this[prop] ){
            this.input[prop] = this[prop];
        }
    }

    _getLabelText() {
        if( !this._labelSlot ) {
            return "";
        }
        let labelText = "";
        let textNodes = this._labelSlot.assignedNodes();
        if( textNodes.length > 0 ) {
            textNodes.forEach( ( n ) => {
                if( n.outerHTML ) {
                    labelText += n.outerHTML;
                } else {
                    labelText += n.nodeValue;
                }
            } );
        }
        return labelText;
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    _validateString( newV ) {
        return typeof newV === "string";
    }

    __updateAttributes( input, label ) {       
        if( !input || !label ) {
            return;
        }
        let attributes = this.attributes;
        for( var i = 0; i < attributes.length; i++ ) {
            let attribute = attributes[i];
            if( attribute.name === 'part' ) {
                continue;
            }
            this._addAttribute( input, label, attribute );
        }
        let oldAttributes = input.attributes;
        for( var i = 0; i < oldAttributes.length; i++ ) {
            let attribute = oldAttributes[i];
            if( !attributes.getNamedItem( attribute.name ) && attribute.name !== 'part' ) {
                this._removeAttribute( input, label, attribute );
            }            
        }
    }

    _notifyParent() {
        this.dispatchEvent(
            new CustomEvent( 'od-form-element-get-parent', { 
                bubbles: true, 
                detail: { 
                    'odFormElement': this,
                    'labelText': this.labelText,
                    'callback': ( parent ) => {
                        this._parent = parent;
                        this._dispatchParentDone( this._parent );
                    } 
                }
            } )
        );
    }

    _dispatchParentDone( parent ) {
        this.dispatchEvent(
            new CustomEvent( 'od-form-element-get-parent-done', { 
                bubbles: true, 
                detail: { 
                    'parent': parent
                }
            } )
        );
    }

    async _parentPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-form-element-get-parent-done', ( e ) => { this._parentPromiseCallback( e, resolve ); } );
        } );
    }

    _parentPromiseCallback( e, resolve ) {
        this.removeEventListener( 'od-form-element-get-parent-done', ( e ) => { this._parentPromiseCallback( e, resolve ); } );
        resolve( e.detail.parent ); 
    }

    async _parentSetInline( newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent.setInline( this.getAttribute( 'name' ), newValue ); 
    }

    async _parentSetChecked( newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent.setChecked( this.getAttribute( 'name' ), this.value, newValue ); 
    }

    async _parentSetRequired( newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent._setRequired( this.getAttribute( 'name' ), newValue );
    }

    async _parentSetHidden( newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent._setHidden( this.getAttribute( 'name' ), newValue );
    }

    async _parentSetFieldAttribute( attrName, newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }           
        this._parent._setFieldAttribute( this.getAttribute( 'name' ), attrName, newValue );
    }

    async _parentSetValue( newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent.setValue( this.getAttribute( 'name' ), newValue );   
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
    }

    get value() {
        return this.getAttribute( "value" );
    }

    set value( value ) {
        this._setValueAttribute( value );
    }

    _setValueAttribute( newV ) {
        if ( !newV || !newV.toString() ) {
            this.removeAttribute( "value" );
        } else {
            this.setAttribute( "value", newV.toString() );
        }
    }    

    get checked() {
        return this.hasAttribute( "checked" );
    }

    set checked( isChecked ) {
        if ( typeof isChecked !== "boolean" ) {
            return;
        }
        this._setCheckedAttribute( isChecked );
    }

    _setCheckedAttribute( newV ) {
        this._setBooleanAttribute( 'checked', newV );
    }

    _setRequiredAttribute( newV ) {
        this._setBooleanAttribute( 'required', newV );
    }

    _setHiddenAttribute( newV ) {
        this._setBooleanAttribute( 'hidden', newV );
    }

    get input() {
        return this._input;
    }

    get labelText() {
        return this._labelText;
    }

    set labelText( newText ) {
        if( this.labelText !== newText ) {
            this.dispatchEvent(
                new CustomEvent( 'od-form-label-update', { 
                    bubbles: true, 
                    detail: {
                        'element': this,
                        'newText': newText
                    }
                } )
            );
        }
        this._labelText = newText;
    }

    async listen( eventName, callback ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent._addFieldListener( this.getAttribute( 'name' ), eventName, callback );
    }
}