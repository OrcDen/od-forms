import OdFormElement from './odFormElement.js'
export default class odFormMultiselectElement extends OdFormElement {

    constructor( template ) {
        super( template );
    }
    connectedCallback() {
        super.connectedCallback();
        this._upgradeProperty( "optionsGet" );        
        this._upgradeProperty( "optionsValue" );
        this._upgradeProperty( "optionsLabel" );
        this._addSlotObserver()
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        return;
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "options-get": {
                if ( newValue !== null && !this._validateString( newValue ) ) {
                    this._setOptionsGetAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setOptionsGetAttribute( newValue );
                    this.getMultiSelectOptions( this.tagName );
                }
                return;
            }
            case "options-label": {
                if ( newValue !== null && !this._validateString( newValue ) ) {
                    this.setOptionsLabelAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setOptionsLabelAttribute( newValue );
                    this.getMultiSelectOptions( this.tagName );
                }
                return;
            }
            case "options-value": {
                if ( newValue !== null && !this._validateString( newValue ) ) {
                    this._setOptionsValueAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setOptionsValueAttribute( newValue );
                    this.getMultiSelectOptions( this.tagName );
                }
                return;
            }
            case "disabled": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setDisabledAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setDisabledAttribute( newValue );

                    this._parentSetGroupBool( 'disabled', newValue );   
                }
                return;
            }
            case "required": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setRequiredAttribute( oldValue );
                    return;
                }
                if( newValue !== oldValue ) {
                    this._setRequiredAttribute( newValue );

                    this._parentSetRequired( newValue );
                    this._parentSetGroupBool( 'required', newValue );  
                }
                return;
            }
        }
        super.attributeChangedCallback( attrName, oldValue, newValue );
    }

    _addSlotObserver() {
        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {
            mutationsList.forEach( record => {
                let addedNodes = record.addedNodes;
                addedNodes.forEach( node => {
                    let option = {};
                    option['label'] = node.innerHTML;
                    option['value'] = node.value;
                    this._parentAddOption( option );
                } )
            });
        } );
        this._observer.observe( this, config );
    }

    async _parentAddOption( option ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        let type = this._getType( this.tagName )
        this._parent._addMultiSelectOption( option, this.getAttribute( 'name' ), type, this.getAttribute( 'name' ), this.hasAttribute( 'required' ), 'label', 'value' );
    }

    _setDisabledAttribute( newV ) {
        this._setBooleanAttribute( 'disabled', newV );
    }

    async _parentSetGroupBool( attrName, newValue ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent._setGroupBooleanAttribute( this.getAttribute( 'name' ), attrName, newValue );
    }

    get optionsGet() {
        return this.getAttribute( "options-get" );
    }

    set optionsGet( urlString ) {
        if ( typeof isInline !== "string" ) {
            return;
        }
        this._setOptionsGetAttribute( urlString );
    }

    _setOptionsGetAttribute( newV ) {
        if ( !newV ) {
            this.removeAttribute( "options-get" );
        } else {
            this.setAttribute( "options-get", newV );
        }
    }

    get optionsValue() {
        return this.getAttribute( "options-value" );
    }

    set optionsValue( valueString ) {
        if ( typeof valueString !== "string" ) {
            return;
        }
        this._setOptionsValueAttribute( valueString );
    }

    _setOptionsValueAttribute( newV ) {
        if ( !newV ) {
            this.removeAttribute( "options-value" );
        } else {
            this.setAttribute( "options-value", newV );
        }
    }

    get optionsLabel() {
        return this.getAttribute( "options-label" );
    }

    set optionsLabel( labelString ) {
        if ( typeof labelString !== "string" ) {
            return;
        }
        this._setOptionsLabelAttribute( labelString );
    }

    _setOptionsLabelAttribute( newV ) {
        if ( !newV ) {
            this.removeAttribute( "options-label" );
        } else {
            this.setAttribute( "options-label", newV );
        }
    }

    get value() {
        return this.getValue();
    }

    set value( value ) {
        this.setValue( value );
    }

    async getMultiSelectOptions( tagName ) {
        if( !this.optionsGet || !this.optionsValue || !this.optionsLabel ) {
            return;
        }
        let type = this._getType( tagName );
        this.dispatchEvent(
            new CustomEvent( 'od-form-get-options', { 
                bubbles: true, 
                detail: { 
                    'url': this.optionsGet,
                    'id': this.getAttribute( 'name' ),
                    'type': type,
                    'name': this.getAttribute( 'name' ),
                    'required': this.hasAttribute( 'required' ),
                    'labelField': this.optionsLabel,
                    'valueField': this.optionsValue
                }
            } )
        );
    }

    async listen( eventName, callback ) {
        if( !this._parent ) {
            await this._parentPromise();
        }
        this._parent._addMultiFieldListener( this.getAttribute( 'name' ), eventName, callback );
    }

    _getType( tagName ) {
        let type = null;
        switch( tagName ) {
            case ( 'OD-FORM-SELECTLIST' ) : {
                type = 'select';
                break;
            }
            case ( 'OD-FORM-MULTISELECT' ) : {
                type = 'checkbox';
                break;
            }
            case ( 'OD-FORM-RADIOSELECT' ) : {
                type = 'radio';
                break;
            }
            default : {
                break
            }
        }
        return type;
    }
}