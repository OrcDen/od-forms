export default {
    testServer: 'http://localhost:8079',
    name: 'OD Form',
    componentTag: 'od-form',
    componentSelector: 'od-form',
    hasShadow: true,
    shadowSelector: '#od-form1',
    properties: [
        {
            name: 'disabled',
            type: Boolean,
            default: false
        }
    ]
}