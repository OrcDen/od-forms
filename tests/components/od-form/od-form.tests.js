import { Selector, ClientFunction } from 'testcafe';
import { TestCafeComponentTestSuite } from '@orcden/od-component-test-suite';
import _config from './config';

const testSuite = new TestCafeComponentTestSuite( _config );
testSuite.runAllTests();

const model = testSuite.model;
const topButton = model.component;

fixture( 'OD-Top-Button Custom Tests' )
    .page( model.testServer );
